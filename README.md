# PasswordGuesser

## Définition

__Visibilité (l'encapsulation)__ 

En programmation orientée objet, la visibilité fait référence à la portée et à l'accessibilité des membres d'une classe (variables, méthodes, etc.) à d'autres parties du programme. Elle définit les règles qui déterminent quelles parties du code peuvent accéder et manipuler les membres d'une classe.

1. __Public__ : [Exemple (methode get_lower_words)](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/lower.py#L4)

Un membre déclaré comme public est accessible depuis n'importe quelle partie du programme, que ce soit à l'intérieur de la classe, dans les classes dérivées (sous-classes) ou depuis d'autres classes. Il n'y a pas de restriction d'accès.

2. __Privé__ : [Exemple (attribut __words)](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/lower.py#L3)

Un membre déclaré comme privé est accessible uniquement à l'intérieur de la classe où il est défini. Il n'est pas accessible depuis les classes dérivées ou depuis d'autres classes. Les membres privés sont souvent utilisés pour encapsuler les détails d'implémentation d'une classe et pour restreindre l'accès direct aux données.

3. __Protected__ : [Exemple (attribut self._word = _word)](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/unicode.py#L5)

Un membre déclaré comme protégé est accessible à l'intérieur de la classe où il est défini, ainsi que dans les classes dérivées (sous-classes) de cette classe. Il n'est pas accessible depuis d'autres classes en dehors de la hiérarchie d'héritage. Le niveau de visibilité protégé est souvent utilisé pour permettre l'accès contrôlé aux membres d'une classe par ses sous-classes.

__Polymorphisme__ : [Exemple (def get_leet_indexes_word(self)):](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/lower.py#L12)

Le polymorphisme permet à un objet d'être utilisé de différentes manières en fonction du contexte. Il permet de traiter des objets de différentes classes de manière uniforme en utilisant des méthodes communes. Cela est possible grâce à l'héritage et au lien dynamique des méthodes. C'est la surcharge d'une méthode. C'est à dire la modification de se signature (nom de la méthode, les paramètres ou le type de retour).

__La composition__ : [Exemple (Upper(self.words))](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/options.py#L31)

La composition consiste à combiner des objets les uns à l'intérieur des autres pour former des structures complexes. Elle permet à un objet de contenir d'autres objets en tant que membres pour utiliser leurs fonctionnalités. Cela offre flexibilité, réutilisation du code et possibilité de créer des structures dynamiques.

__L'héritage__ : [Exemple (class Leet(Lower):))](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/leet.py#L3)

L'héritage est un mécanisme qui permet à une classe d'acquérir les propriétés et les fonctionnalités d'une autre classe. La classe qui hérite, appelée classe enfant ou classe dérivée, peut utiliser les attributs et les méthodes de la classe parent ou classe de base, tout en ayant la possibilité de les modifier ou d'en ajouter de nouveaux. L'héritage favorise la réutilisation du code et facilite la création de hiérarchies de classes pour organiser et structurer le code de manière logique.

__Une interface__ (ici classe Abstraite car pas d'interfacec en pyhton ) : [Exemple (class DefaultDate(ABC):))](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Dates/default_date.py#L3)

Une interface est une structure qui définit un ensemble de méthodes ou de comportements attendus d'une classe. Elle sert de contrat entre les classes qui l'implémentent et garantit la disponibilité des méthodes spécifiées. Une interface ne contient pas d'implémentation concrète de ces méthodes, mais fournit une abstraction permettant de définir des fonctionnalités communes à plusieurs classes. Les classes qui implémentent une interface doivent fournir une implémentation pour chacune de ses méthodes, ce qui permet de réaliser une polymorphie et de favoriser la modularité et l'extensibilité du code.

__Méthodes et attributs d'objets__ : [Exemple (def get_leet_words(self):))](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/leet.py#L22)

C'est une méthode qui a accès aux méthodes et attribues de la classe. Cependant il est obligatoire d'avoir instancié un objet de cette classe pour pouvoir l'utiliser.

__Méthodes et attributs statiques__ : [Exemple (def get_unicode_words(words):))](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/unicode.py#L9)

C'est une méthode qui n'à accès a aucune méthode ou attribue de la classe. Cette méthode peut-etre utilisée sans avoir besoin d'instancier un objet de cette classe.

__Méthodes et attributs de classe__ : [Exemple (def get_capitalize_words(cls, words):))](https://gitlab.com/Corentin_gc/passwordguesser/-/blob/main/back/Class/Words/capitalize.py#L7)

C'est une méthode qui a accès aux méthodes et attribues de la classe. Cette méthode peut-etre utilisée sans avoir besoin d'instancier un objet de cette classe.
import unidecode

class Unicode:
    def __init__(self, __words):
        self.__words = __words
        self.possibilities = self.get_unicode_words(__words)

    @staticmethod
    def get_unicode_words(words):
        unicode_words = []
        for word in words:
            unicode_words.append(unidecode.unidecode(word))
        return unicode_words        
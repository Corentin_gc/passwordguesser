class Capitalize:
    def __init__(self, words):
        self.words = words
        self.possibilities = self.get_capitalize_words(words)

    @classmethod
    def get_capitalize_words(cls, words):
        capitalize_words = []
        for word in words:
            capitalize_words.append(word.capitalize())
        return capitalize_words        
from Class.Words.lower import Lower

class Leet(Lower):
    leet_letters = {
        "a" : 4,
        "e" : 3,
        "i" : 1,
        "o" : 0,
        "l" : 1,
        "s" : 5,
        "b" : 8,
        "t" : 7,
        "z" : 2,
        "g" : 6
    }

    def __init__(self, __words, options):
        self.__words = __words
        self.options = options
        self.possibilities = self.get_leet_words()

    def get_leet_words(self):
        leet_words = []
        for word in self.__words:
            self.get_leet_possibilities(word, leet_words)
        return leet_words

    def get_leet_possibilities(self, word, leet_words):
        leet_indexes = self.get_leet_indexes_word(word)
        possibilities = self.get_all_combinaisons(leet_indexes)
        
        for possibility in possibilities:
            if len(possibility) > 0 :
                leet_word = word
                for index in possibility:
                    leet_word = leet_word[:index] + str(self.leet_letters[word[index]]) + leet_word[index+1:]
                leet_words.append(leet_word)
    
    def get_leet_indexes_word(self, word):
        indexes_found = []
        for element in range(0, len(word)):
            if(word[element] in self.leet_letters):
                indexes_found.append(element)
        return indexes_found

    def get_all_combinaisons(self, indexes):
        if len(indexes) == 0:
            return [[]]
        cs = []
        for c in self.get_all_combinaisons(indexes[1:]):
            cs += [c, c+[indexes[0]]]
        return cs

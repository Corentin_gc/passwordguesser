class Upper:
    def __init__(self, words):
        self.words = words
        self.possibilities = self.get_upper_words(words)

    def get_upper_words(self, words):
        upper_words = []
        for word in words:
            upper_words.append(word.upper())
        return upper_words        

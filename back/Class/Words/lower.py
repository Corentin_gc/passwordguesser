class Lower:
    def __init__(self, words):
        self.__words = words
        self.possibilities = self.get_lower_words(words)

    def get_lower_words(self, words):
        lower_words = []
        for word in words:
            lower_words.append(word.lower())
        return lower_words        

    def get_leet_indexes_word(self):
        return null

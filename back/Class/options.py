# import Words
from Class.Words.lower import Lower
from Class.Words.upper import Upper
from Class.Words.capitalize import Capitalize
from Class.Words.unicode import Unicode
from Class.Words.leet import Leet

# import Dates
from Class.Dates.default_date import DefaultDate
from Class.Dates.human_date import HumanDate

class Options:
    def __init__(self, options, words, dates, array_possibility, array_charsets):
        self.options = options
        self.words = words
        self.dates = dates
        self.array_possibility = array_possibility 
        self.array_charsets = array_charsets 
        self.leet_options = {
            "leet_one_letter" : self.options["leet_one_letter"],
            "leet_all_combinaisons" : self.options["leet_all_combinaisons"],
            "leet_all_letters" : self.options["leet_all_letters"]
        }      
        self.manage_options()

    def manage_options(self):
        self.array_possibility.extend(DefaultDate(self.dates, self.options["two_number_year"]).possibilities)
        if self.options["lower_words"]:
            self.array_possibility.extend(Lower(self.words).possibilities)
        if self.options["upper_words"]:
            self.array_possibility.extend(Upper(self.words).possibilities)
        if self.options["capitalize_words"]:
            self.array_possibility.extend(Capitalize(self.words).possibilities)
        if self.options["unicode_words"]:
            self.get_unicode_words()
        if self.options["human_date"]:
            self.array_possibility.extend(HumanDate(self.dates).possibilities)
        if self.options["leet_one_letter"] or self.options["leet_all_combinaisons"] or self.options["leet_all_letters"]:
            if self.array_possibility:
                self.array_possibility.extend(Leet(self.array_possibility, self.leet_options).possibilities)
            else:
                self.array_possibility.extend(Leet(self.words, self.leet_options).possibilities)
        if self.options["common_charsets"]:
            self.array_charsets.extend((".", "$", "?", "!", "*"))
        if self.options["all_charsets"]:
            self.array_charsets.extend((".", "$", "?", "!", "*", '~', ':', "'", '+', '[', '\\', '@', '^', '{', '%', '(', '-', '"', '|', ',', '&', '<', '`', '}', '_', '=', ']', '!', '>', ';', '#', ')', '/'))


    def get_unicode_words(self):
        unicode_words = Unicode(self.words).possibilities
        self.array_possibility.extend(unicode_words)
        self.array_possibility.extend(Lower(unicode_words).possibilities)
        self.array_possibility.extend(Upper(unicode_words).possibilities)
        self.array_possibility.extend(Capitalize(unicode_words).possibilities)
        
        

            
        

import locale

class HumanDate:
    def __init__(self, dates):
        self.dates = dates
        self.possibilities = self.get_human_dates(dates)

    def get_human_dates(self, dates):
        human_dates = []
        for date in dates:
            locale.setlocale(locale.LC_TIME,'fr_FR')
            human_dates.append(date.strftime("%A"))
            human_dates.append(date.strftime("%A").upper())
            human_dates.append(date.strftime("%A").capitalize())
            human_dates.append(date.strftime("%B")) 
            human_dates.append(date.strftime("%B").upper()) 
            human_dates.append(date.strftime("%B").capitalize())
        return human_dates        
from abc import ABC

class DefaultDate(ABC):
    def __init__(self, dates, two_number_year):
        self.dates = dates
        self.two_number_year = two_number_year
        self.possibilities = self.get_default_dates(dates)

    def get_default_dates(self, dates):
        default_dates = []
        for date in dates:
            default_dates.append(date.strftime("%#d"))
            default_dates.append(date.strftime("%d"))
            default_dates.append(date.strftime("%m"))
            default_dates.append(date.strftime("%#m"))
            default_dates.append(date.strftime("%Y"))
            if self.two_number_year:
                default_dates.append(date.strftime("%y"))
        return default_dates  
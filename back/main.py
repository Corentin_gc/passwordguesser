from Class.options import Options
import datetime

class Engine :
    def __init__(self):
        self.array_possibility = []
        self.array_charsets = []
        self.run()

    def run(self):
        options = {
            "lower_words" : True,
            "upper_words" : True,
            "capitalize_words" : True,
            "unicode_words" : True,
            "leet_one_letter" : True,
            "leet_all_combinaisons" : True,
            "leet_all_letters" : True,
            "human_date" : True,
            "two_number_year" : True,
            "common_charsets" : True,
            "all_charsets" : True
        }

        words = ['alexandre' ]
        dates = []
        # dates = [datetime.date(2023, 5, 3)]
        #words = ['jackii', 'tété', 'toto', 'Corentin', 'éoliène']

        # Instanciation des options
        manage_options = Options(options, words, dates, self.array_possibility, self.array_charsets)

        # On récupère les mots et dates possibles et on supprime les doublons 
        possibilities = list(dict.fromkeys(manage_options.array_possibility))

        # S'il y a on récupère les caractères spéciaux
        charsets = manage_options.array_charsets
        
        # Récupération des combinaisons possibles 
        result = self.password_possibles(possibilities+charsets)
        print(result)
        print(len(result))

    def password_possibles(self, elements, max_lenght=5) :
        words = set()
        n = len(elements)

        # Générer toutes les combinaisons de mots de longueur 1 à n
        for i in range(1, min(n, max_lenght)+1):
            for j in range(n-i+1):
                mot = elements[j:j+i]
                words.add(''.join(mot))

        return words

if __name__ == '__main__':
    Engine()